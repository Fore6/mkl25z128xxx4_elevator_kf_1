/*
 * common.h
 *
 *  Created on: 3. 1. 2019
 *      Author: pc
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <stdio.h>
#include <string.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
#include "fsl_pit.h"

#include "ELEVATOR/defines.h"


#endif /* COMMON_H_ */
