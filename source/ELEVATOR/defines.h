/*
 * DEFINES.h
 *
 *  Created on: 3. 1. 2019
 *      Author: pc
 */

#ifndef ELEVATOR_DEFINES_H_
#define ELEVATOR_DEFINES_H_

#include "comands.h"

#define FALSE 0
#define TRUE 1

#define PROTECT_MODE FALSE

#define FLOORS 5
#define MESSAGE_BUFFER_SIZE 256
#define MY_ADDRESS 0x01
#define START_MESSAGE 0xA0
#define MAX_MESSAGE_SIZE MESSAGE_BUFFER_SIZE+6
#define DEFAULT_SPEED 50

#define ELEVATOR_CABIN 0xF0
#define ELEVATOR_MOTOR 0xF1
#define ELEVATOR_TERMINAL 0xD0
#define ELEVATOR_DISPLAY 0x30
#define ELEVATOR_EMERGENCY_BREAK 0x0F
#define ELEVATOR_WATCHDOG_TIMER 0xFE

#define BUTTON_HALL_4 0xC4
#define BUTTON_HALL_3 0xC3
#define BUTTON_HALL_2 0xC2
#define BUTTON_HALL_1 0xC1
#define BUTTON_HALL_P 0xC0
#define BUTTON_CABIN_4 0xB4
#define BUTTON_CABIN_3 0xB3
#define BUTTON_CABIN_2 0xB2
#define BUTTON_CABIN_1 0xB1
#define BUTTON_CABIN_P 0xB0

#define LED_HALL_4 0x14
#define LED_HALL_3 0x13
#define LED_HALL_2 0x12
#define LED_HALL_1 0x11
#define LED_HALL_P 0x10
#define LED_CABIN_4 0x24
#define LED_CABIN_3 0x23
#define LED_CABIN_2 0x22
#define LED_CABIN_1 0x21
#define LED_CABIN_P 0x20

#define LIMIT_SWITCH_4 0xE4
#define LIMIT_SWITCH_3 0xE3
#define LIMIT_SWITCH_2 0xE2
#define LIMIT_SWITCH_1 0xE1
#define LIMIT_SWITCH_P 0xE0

#define WATCHDOG_PERIODE_TIME_MS 500

enum floors{
	floor_P,
	floor_1,
	floor_2,
	floor_3,
	floor_4
};

/**
 * direction of movement elevator
 */
enum direction{
	direction_up = 0x01, //up
	direction_down, //down
	direction_none, //stop
	direction_pauseUp,
	direction_pauseDown
};

enum timerChannels {
	channel_door, channel_watchdog
};



#endif /* ELEVATOR_DEFINES_H_ */
