/*
 * MOTOR.cpp
 *
 *  Created on: 6. 1. 2019
 *      Author: pc
 */

#include <ELEVATOR/MOTOR.h>

MOTOR::MOTOR() {
	speed = DEFAULT_SPEED;
}

MOTOR::~MOTOR() {
	// TODO Auto-generated destructor stub
}

void MOTOR::stop(){
	comunicator.sendComand(ELEVATOR_MOTOR, com_mot_stop);
}

void MOTOR::goDown(){
	uint8_t down[]= {com_mot_movement, 0xFF,0xFF,0xFF, ~(speed-1) };
	comunicator.sendMessage(ELEVATOR_MOTOR, down, 5);
}

void MOTOR::goUp(){
	uint8_t up[]= {com_mot_movement, 0x00,0x00,0x00, speed};
	comunicator.sendMessage(ELEVATOR_MOTOR, up, 5);
}

void MOTOR::set_speed(uint8_t newSpeed) {
	speed = newSpeed;
}

void MOTOR::getPositon(){
	//goDown();
	//for(int i=0; i<0xFFFFFF; i++);
	//stop();
	//for(int i=0; i<0xFFFFFF; i++);
	comunicator.sendComand(ELEVATOR_MOTOR,com_mot_sendMotorEncoderCount);

	/////////////////////////////////////////////////////////////////////
	uint8_t data[MESSAGE_BUFFER_SIZE];
	uint8_t sourceAdr;
	uint8_t len;
	while(sourceAdr != ELEVATOR_MOTOR) {
		len = comunicator.receivedMessage(data, &sourceAdr);
	}

	double* doublecounterfromzero = (double*) data;
	double* doublecounterfromone = (double*) (&data[1]);
	double doublefromzero = *doublecounterfromzero;
	double doublefromone = *doublecounterfromone;
	int a = 0;

}

