/*
 * CABIN.cpp
 *
 *  Created on: 6. 1. 2019
 *      Author: pc
 */

#include <ELEVATOR/CABIN.h>

CABIN::CABIN() {
	// TODO Auto-generated constructor stub

}

CABIN::~CABIN() {
	// TODO Auto-generated destructor stub
}

void CABIN::lockDoor(){
	comunicator.sendComand(ELEVATOR_CABIN, com_cab_lockDoor);
	isLock = TRUE;
}

void CABIN::OpenDoor(){
	comunicator.sendComand(ELEVATOR_CABIN, com_cab_unlockDoor);
		isLock = FALSE;
}

uint8_t CABIN::get_doorStatus() {
	return isLock;
}

