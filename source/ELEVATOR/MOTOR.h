/*
 * MOTOR.h
 *
 *  Created on: 6. 1. 2019
 *      Author: pc
 */

#ifndef ELEVATOR_MOTOR_H_
#define ELEVATOR_MOTOR_H_

#include "PROTOCOl/MY_PROTOCOL.h"

class MOTOR {
private:
	MY_PROTOCOL comunicator;
	uint8_t speed;
public:
	MOTOR();
	virtual ~MOTOR();

	void stop();
	void goDown();
	void goUp();
	void set_speed(uint8_t newSpeed);
	void getPositon();
};

#endif /* ELEVATOR_MOTOR_H_ */
