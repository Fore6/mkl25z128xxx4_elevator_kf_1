/*
 * enumes.h
 *
 *  Created on: 5. 1. 2019
 *      Author: pc
 */

#ifndef ELEVATOR_COMANDS_H_
#define ELEVATOR_COMANDS_H_


enum cabin_comand {
	com_cab_unlockDoor, // (0x00) unlock the door
	com_cab_lockDoor, // (0x01) lock the door
	com_cab_sendDoorStatus // (else = 0x02) send cabin door status (0=unlock, 1=lock)
};

enum emergency_break_comand {
	com_emb_deactivateBreak, // (0x00) deactivate emergency break
	com_emb_activateBreak // (0x01) activate emergency break
};

/**
 * packet structure = { [START_BIT],[ELEVATOR_DISPLAY],[SOURCE_ADDR],[LENGTH],[MOVE_DIRECTIVE],[STRING_TO_PRINT(0)]...[STRING_TO_PRINT(n)], [CRC] }
 */
enum information_display_comand {
	com_dsp_up = 0x01, // (0x01) turn on UP arrow
	com_dsp_down, // (0x02) turn on DOWN arrow
	com_dsp_none // (else = 0x03) turn off arrows
};

enum motr_comand {
	com_mot_stop = 0x01, // (0x01) stop motor
	com_mot_movement, // (0x02) turn into motion motor
	com_mot_sendMotorEncoderCount // (0x03) send motor encoder count
};

enum led_comand {
	com_led_off, // (0x00) turn off led
	com_led_on // (0x01) turn on led
};

enum limit_switch_comand {
	com_lsw_switchLow, // (0x00) cabin on switch
	com_lsw_wideProximity, // (0x01) cabin is close
	com_lsw_narrowProximity, // (0x02) cabin is very close
	com_lsw_request
};

enum watchdog_timer_comand{
	com_wdt_timeOutResetTimer = 0x01, // (0x01) Reset watchdog timer after "timeing out"(restore watchdog timer to initial state))
	com_wdt_regularyResetTimer // (else = 0x02) Regularly resets the watchdog timer to prevent it from elapsing.
};


#endif /* ELEVATOR_COMANDS_H_ */
