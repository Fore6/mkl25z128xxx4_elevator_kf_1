/*
 * CABIN.h
 *
 *  Created on: 6. 1. 2019
 *      Author: pc
 */

#ifndef ELEVATOR_CABIN_H_
#define ELEVATOR_CABIN_H_

#include "PROTOCOl/MY_PROTOCOL.h"

class CABIN {
private:
	MY_PROTOCOL comunicator;
	uint8_t isLock;
public:
	CABIN();
	virtual ~CABIN();
	void lockDoor();
	void OpenDoor();
	uint8_t get_doorStatus();
};

#endif /* ELEVATOR_CABIN_H_ */
