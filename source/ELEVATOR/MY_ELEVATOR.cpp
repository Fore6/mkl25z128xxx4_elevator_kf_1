/*
 * MY_ELEVATOR.cpp
 *
 *  Created on: 3. 1. 2019
 *      Author: pc
 */

#include <ELEVATOR/MY_ELEVATOR.h>

MY_ELEVATOR::MY_ELEVATOR() {

	watchdog = new WATCHDOG(&timer, WATCHDOG_PERIODE_TIME_MS, channel_watchdog);

	floor = floor_4;
	direction = direction_none;
	inMove = FALSE;
	powerOn = TRUE;
	emergencyBreakActiv = FALSE;


	timer.startTimer();

		//DOCASU////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//while(1) {
			//GETCHAR();
			//for(int i; i < 0xFFFFF;i++);
		//	GETCHAR();
		//	int b = timer.getMyTimerCounter();
		//	PRINTF("-* %d --elevator timer\n\r", b);
		//}

		//while(1){
		//	watchdog->controlWatchdog();
		//}
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	//goTo_defaultPosition();

	//motor.getPositon();

}

MY_ELEVATOR::~MY_ELEVATOR() {
	delete watchdog;
}

uint8_t MY_ELEVATOR::controlWatchdog(){
	return watchdog->controlWatchdog();
}

uint8_t MY_ELEVATOR::controlMove(){
	switch (direction) {
	case direction_down:

		break;
	case direction_up:

		break;
	case direction_pauseDown:

		break;
	case direction_pauseUp:

		break;
	case direction_none:

		break;
	default:
		break;

	}
		//control display there
}

uint8_t MY_ELEVATOR::controlReceiveComand(){

}

uint8_t MY_ELEVATOR::startMove(){

}

uint8_t MY_ELEVATOR::goTo_defaultPosition() {

	comunicator.sendComand(LIMIT_SWITCH_P, com_lsw_request);

	do {
		comunicator.receivedMessage(inData, &receivedSource);
	} while (receivedSource != LIMIT_SWITCH_P);

	if(inData[0] == com_lsw_switchLow) {

		inData[0] += 'A';
		//comunicator.sendMessage(ELEVATOR_TERMINAL, inData, 1);

		cabin.lockDoor();
		motor.goDown();
		inMove = TRUE;
		do {
			comunicator.receivedMessage(inData, &receivedSource);
		} while (receivedSource != LIMIT_SWITCH_P);
		motor.stop();
		inMove = FALSE;
	}

	floor = floor_P;
	set_display(direction_none, floor);
	return 0;


}


uint8_t MY_ELEVATOR::set_nextFloor() {
	if(direction == direction_down){
		floor--;
	} else if (direction == direction_up) {
		floor++;
	}
	return floor;
}

void MY_ELEVATOR::set_display(uint8_t direction, uint8_t floor) {
	if(floor == floor_P) floor = 'P';
	uint8_t data[] = {direction, floor};
	comunicator.sendMessage(ELEVATOR_DISPLAY, data, 2);
}

void MY_ELEVATOR::set_led(uint8_t led_address, uint8_t led_status) {
	comunicator.sendComand(led_address, led_status);
}


uint8_t MY_ELEVATOR::move(uint8_t move) {

	switch (move) {

	case direction_none:
	case direction_pauseDown:
	case direction_pauseUp:
		motor.stop();
		set_display(com_dsp_none, set_nextFloor());
		direction= direction_none;
		break;

	case direction_up:
		//osetrit ci vytah stoji
		motor.goUp();
		set_display(direction_up, floor);
		direction = direction_up;
		break;

	case direction_down:
		//osetrit ci vytah stoji
		motor.goDown();
		set_display(direction_down, floor);
		direction = direction_down;
		break;

	default:
		return 1;
		break;
	}
	return 0;
}



uint8_t MY_ELEVATOR::set_emergencyBreak(uint8_t active) {
	emergencyBreakActiv = active;
	if (active) {
		comunicator.sendComand(ELEVATOR_EMERGENCY_BREAK, com_emb_activateBreak);
	} else {
		comunicator.sendComand(ELEVATOR_EMERGENCY_BREAK, com_emb_deactivateBreak);
	}
	return 0;
}
