/*
 * WATCHDOG.h
 *
 *  Created on: 9. 1. 2019
 *      Author: pc
 */

#ifndef ELEVATOR_WATCHDOG_H_
#define ELEVATOR_WATCHDOG_H_

#include "PROTOCOl/MY_PROTOCOL.h"
#include "TIMER/MY_TIMER.h"

class WATCHDOG {
private:
	MY_PROTOCOL comunicator;
	MY_TIMER* timer;
	int periodeTime_ms;
	uint8_t timerChannel;
	uint8_t isBlock;
public:
	WATCHDOG();
	WATCHDOG(MY_TIMER* timer, int periodeTime_ms, uint8_t timerChannel);
	virtual ~WATCHDOG();

	/**
	 * @return 0 command for regular Reset was send
	 * @return 1 command for regular Reset was not send
	 */
	uint8_t controlWatchdog();
	void regularyReset();
	void timeOutReset();
};

#endif /* ELEVATOR_WATCHDOG_H_ */
