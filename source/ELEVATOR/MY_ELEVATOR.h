/*
 * MY_ELEVATOR.h
 *
 *  Created on: 3. 1. 2019
 *      Author: pc
 */

#ifndef ELEVATOR_MY_ELEVATOR_H_
#define ELEVATOR_MY_ELEVATOR_H_

#include "common.h"
#include "PROTOCOl/MY_PROTOCOL.h"
#include "ELEVATOR/CABIN.h"
#include "ELEVATOR/MOTOR.h"
#include "ELEVATOR/WATCHDOG.h"
#include "TIMER/MY_TIMER.h"



class MY_ELEVATOR {
private:
	MY_PROTOCOL comunicator;
	CABIN cabin;
	MOTOR motor;
	MY_TIMER timer;
	WATCHDOG* watchdog;


	uint8_t inData[MESSAGE_BUFFER_SIZE];
	uint8_t outData[MESSAGE_BUFFER_SIZE];
	uint8_t receivedSource;
	uint8_t floorRequests[FLOORS];
	uint8_t floor;
	uint8_t direction;
	uint8_t inMove;
	uint8_t powerOn;
	uint8_t emergencyBreakActiv;
public:

	MY_ELEVATOR();
	virtual ~MY_ELEVATOR();

	/**
	 * function active emergency break
	 */
	uint8_t set_emergencyBreak(uint8_t active);

	/**
	 * send comand for reset watchdog periodic
	 */
	uint8_t controlWatchdog();

	/**
	 * @return 0 when success start move,
	 * @return 1 when is not request for move
	 * @return 2 when move is block by timer
	 */
	uint8_t controlMove();

	uint8_t controlReceiveComand();



private:

	uint8_t startMove();


	uint8_t goTo_defaultPosition();

	/**
	 * @return 	floor+1 when direction is UP
	 * 			floor-1 when direction is DOWN
	 * 			floor when direction is NONE or stop
	 */
	uint8_t set_nextFloor();

	void set_display(uint8_t direction, uint8_t floor);

	void set_led(uint8_t led_address, uint8_t led_status);


	/**
	 * function start or stop move if elevator is regularz on some floor
	 */
	uint8_t move(uint8_t move);

	/**
	 * function set floorRequest flag byte
	 */
	void set_floorRequest(uint8_t);

	/**
	 * read message and do reaction
	 */
	void action();

};


#endif /* ELEVATOR_MY_ELEVATOR_H_ */
