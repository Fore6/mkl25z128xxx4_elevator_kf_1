/*
 * WATCHDOG.cpp
 *
 *  Created on: 9. 1. 2019
 *      Author: pc
 */

#include <ELEVATOR/WATCHDOG.h>

WATCHDOG::WATCHDOG() {
	// TODO Auto-generated constructor stub

}

WATCHDOG::WATCHDOG(MY_TIMER* timer_p, int periodeTime_ms_p, uint8_t timerChannel_p) {
	timer = timer_p;
	periodeTime_ms = periodeTime_ms_p;
	timerChannel = timerChannel_p;
	timer->setChannelTimer(timerChannel, periodeTime_ms);
}

WATCHDOG::~WATCHDOG() {
	// TODO Auto-generated destructor stub
}

uint8_t WATCHDOG::controlWatchdog() {
	if(timer->isTimerExpired(timerChannel)) {
		regularyReset();
		timer->setChannelTimer(timerChannel, periodeTime_ms);
		return 0;
	}
	return 1;
}

void WATCHDOG::regularyReset(){
	comunicator.sendComand(ELEVATOR_WATCHDOG_TIMER, com_wdt_regularyResetTimer);
}

void WATCHDOG::timeOutReset(){
	comunicator.sendComand(ELEVATOR_WATCHDOG_TIMER, com_wdt_timeOutResetTimer);
	isBlock = FALSE;
}

