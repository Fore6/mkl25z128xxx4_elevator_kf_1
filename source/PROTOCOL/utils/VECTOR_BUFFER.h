/*
 * CIRCULAR_BUFFER.h
 *
 *  Created on: 11. 1. 2019
 *      Author: pc
 */

#ifndef PROTOCOL_UTILS_CIRCULAR_BUFFER_H_
#define PROTOCOL_UTILS_CIRCULAR_BUFFER_H_

#include "common.h"

enum {
	BUFF_EMPTY, BUFF_FREE, BUFF_FULL
};

class INTEGER_8 {
private:
	uint8_t i;
	INTEGER_8* next;
	INTEGER_8* previous;
public:
	INTEGER_8();
	INTEGER_8(uint8_t in);
	INTEGER_8(uint8_t in, INTEGER_8* previous_p);
	virtual ~INTEGER_8();
	void set_i(uint8_t in);
	void setPrevious(INTEGER_8* previous_p);
	void setNext(INTEGER_8* next_p);
	INTEGER_8* getNext();
	uint8_t get_i();
};

//---------------------------------------------------------

class VECTOR_BUFFER {
private:
	INTEGER_8 *buf;
	INTEGER_8 *head;
	INTEGER_8 *tail;
	uint16_t size;

public:
	VECTOR_BUFFER();
	virtual ~VECTOR_BUFFER();

	uint8_t bufferWrite(uint8_t in_char);
	uint8_t bufferRead(uint8_t* out_char);
	uint8_t getSize();

	//void bufferInit(buffer_t* buff, uint8_t* bufferPt, uint16_t size);
	//size_t bufferCapacity(buffer_t* buff);

	//size_t bufferBytesFree(const buffer_t *rb);
};

#endif /* PROTOCOL_UTILS_CIRCULAR_BUFFER_H_ */
