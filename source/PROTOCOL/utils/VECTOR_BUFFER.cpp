/*
 * CIRCULAR_BUFFER.cpp
 *
 *  Created on: 11. 1. 2019
 *      Author: pc
 */

#include <PROTOCOL/utils/VECTOR_BUFFER.h>

INTEGER_8::INTEGER_8(){

}

INTEGER_8::INTEGER_8(uint8_t in){
 i = in;
 previous =  NULL;
 next =  NULL;
}

INTEGER_8::INTEGER_8(uint8_t in, INTEGER_8* previous_p){
 i = in;
 previous =  previous_p;
 next =  NULL;
}

INTEGER_8::~INTEGER_8(){

}

void INTEGER_8::set_i(uint8_t in){
	i = in;
}
uint8_t INTEGER_8::get_i(){
	return i;
}

void INTEGER_8::setPrevious(INTEGER_8* previous_p){
	previous = previous_p;
}

void INTEGER_8::setNext(INTEGER_8* next_p){
	next = next_p;
}

INTEGER_8 *INTEGER_8::getNext(){
	return next;
}


//-----------------------------------------------------

VECTOR_BUFFER::VECTOR_BUFFER() {
	head = NULL;
	tail = NULL;
	size = 0;
	// TODO Auto-generated constructor stub

}

VECTOR_BUFFER::~VECTOR_BUFFER() {
	// TODO Auto-generated destructor stub
}

uint8_t VECTOR_BUFFER::bufferWrite(uint8_t in_char){
	INTEGER_8* in = new INTEGER_8(in_char, head);
	if (size){
		head->setNext(in);
		head = in;
	} else {
		head =in;
		tail = head;
	}
	size++;
	return 0;
}
uint8_t VECTOR_BUFFER::bufferRead(uint8_t* out_char){

	if(!size) return 0;

	INTEGER_8* out;
	if(size > 1) {
		out = tail;
		tail = tail->getNext();
	}else if (size == 1){
		out = tail;
		tail = NULL;
		head = NULL;
	}
	*out_char = out->get_i();
	size--;
	delete out;
	return 1;

}

uint8_t VECTOR_BUFFER::getSize() {
	return size;
}

