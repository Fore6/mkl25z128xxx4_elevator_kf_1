/*
 * MYPROTOCOL.h
 *
 *  Created on: 3. 1. 2019
 *      Author: pc
 */

#ifndef PROTOCOL_MY_PROTOCOL_H_
#define PROTOCOL_MY_PROTOCOL_H_


#include "common.h"

class MY_PROTOCOL {
public:
	MY_PROTOCOL();
	virtual ~MY_PROTOCOL();


	/**
	 * send message to destination
	 */
	void sendMessage(uint8_t destination, uint8_t* data, uint8_t datalen);

	/**
	 *
	 */
	void sendComand(uint8_t destination, uint8_t comand);

	/**
	 *
	 *
	 * @buffer for received message
	 *
	 * @return length of received data
	 * 			if PROTECT_MODE is true return = 0 when calculatedCRC and receivedCRC is not equal
	 */
	uint8_t receivedMessage(uint8_t* data);

	/**
	 * received clear data
	 */
	uint8_t receivedMessage(uint8_t* data, uint8_t* source_address);

private:
	/**
	 * function received ACK
	 * @return 1 when success received ACK, 0 when failure receive ACK
	 */
	uint8_t receiveACK();

	/**
	* function calculate CRC
	 *
	 * @param data data for calculate CRC
	 *
	 * @param length of data
	 */
	uint8_t getCRC(uint8_t* data, uint8_t len);


};


#endif /* PROTOCOL_MY_PROTOCOL_H_ */
