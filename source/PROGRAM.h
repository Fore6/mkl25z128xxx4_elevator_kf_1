/*
 * PROGRAM.h
 *
 *  Created on: 5. 1. 2019
 *      Author: pc
 */

#ifndef PROGRAM_H_
#define PROGRAM_H_
#include "ELEVATOR/MY_ELEVATOR.h"

class PROGRAM {
private:
	MY_ELEVATOR elevator;
	uint8_t running;

public:
	PROGRAM();
	virtual ~PROGRAM();

	/**
	 * function, where is main elevator program
	 */
	uint8_t start();
	uint8_t stop();
	uint8_t freeFallOccured();
private:

};

#endif /* PROGRAM_H_ */
