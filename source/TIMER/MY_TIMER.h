/*
 * MYTIMER.h
 *
 *  Created on: 9. 1. 2019
 *      Author: pc
 */

#ifndef TIMER_MY_TIMER_H_
#define TIMER_MY_TIMER_H_

#include "common.h"
#include "math.h"

#define TIMER_PERIODE_US 50000
#define MAX_CHANNELS 5
//extern "C" void PIT_IRQHandler(void);


class MY_TIMER {
public:
	MY_TIMER();
	virtual ~MY_TIMER();
	void setDoorBlock(int timeBlockDoor_ms);
	void startTimer();
	void setChannelTimer(uint8_t channel, int time_ms);
	int getChannelTimer(uint8_t channel);
	uint8_t isTimerExpired(uint8_t channel);
};

#endif /* TIMER_MY_TIMER_H_ */
