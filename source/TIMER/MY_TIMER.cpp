/*
 * MYTIMER.cpp
 *
 *  Created on: 9. 1. 2019
 *      Author: pc
 */

#include <TIMER/MY_TIMER.h>

int channelTimer[MAX_CHANNELS];

extern "C" void PIT_IRQHandler(void)
{
	/* Clear interrupt flag.*/
	PIT_ClearStatusFlags(PIT, kPIT_Chnl_0, kPIT_TimerFlag);

	for(int i=0; i<MAX_CHANNELS; i++) {
		if(channelTimer[i]) channelTimer[i]--;
	}
}

MY_TIMER::MY_TIMER() {
	pit_config_t pitConfig;
	PIT_GetDefaultConfig(&pitConfig);
	PIT_Init(PIT, &pitConfig);
	PIT_SetTimerPeriod(PIT, kPIT_Chnl_0, USEC_TO_COUNT(TIMER_PERIODE_US, CLOCK_GetFreq(kCLOCK_BusClk)));
	PIT_EnableInterrupts(PIT, kPIT_Chnl_0, kPIT_TimerInterruptEnable);
	EnableIRQ(PIT_IRQn);
	PIT_StartTimer(PIT, kPIT_Chnl_0);
}

MY_TIMER::~MY_TIMER() {
	// TODO Auto-generated destructor stub
}

void MY_TIMER::startTimer(){
	PIT_StartTimer(PIT, kPIT_Chnl_0);
}

void MY_TIMER::setChannelTimer(uint8_t channel, int time_ms) {
	channelTimer[channel] = round( (time_ms*1000) / TIMER_PERIODE_US );
}

int MY_TIMER::getChannelTimer(uint8_t channel) {
	return channelTimer[channel];
}

uint8_t MY_TIMER::isTimerExpired(uint8_t channel){
	if(channelTimer[channel]) return FALSE;
	return TRUE;
}
