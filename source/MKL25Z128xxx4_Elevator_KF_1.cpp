/*
 * Copyright 2016-2018 NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    MKL25Z128xxx4_Elevator_KF_1.cpp
 * @brief   Application entry point.
 */

#include "common.h"
#include "PROGRAM.h"
// DOCASU""""""""""
#include "PROTOCOl/MY_PROTOCOL.h"
#include "ELEVATOR/MY_ELEVATOR.h"
//"""""""""""""""""""

#include<PROTOCOL/utils/VECTOR_BUFFER.h>

void testProgram() {
	LED_RED_INIT(kGPIO_DigitalOutput);


	VECTOR_BUFFER buf;

		buf.bufferWrite('A');
		buf.bufferWrite('H');
		buf.bufferWrite('O');
		buf.bufferWrite('J');
		buf.bufferWrite('a');
		buf.bufferWrite('k');
		buf.bufferWrite('o');
		buf.bufferWrite('s');
		uint8_t c;
		uint8_t success;
		do {
			success = buf.bufferRead(&c);
			if (success) {
				PUTCHAR(c);
				PUTCHAR('\n');
				PUTCHAR('\r');
			}
		} while (success);

		buf.bufferWrite('A');
		buf.bufferWrite('i');
		buf.bufferWrite('d');
		buf.bufferWrite('e');
		buf.bufferWrite('t');
		buf.bufferWrite('o');
		buf.bufferWrite('o');
		buf.bufferWrite('k');

		do {
			success = buf.bufferRead(&c);
			if (success) {
				PUTCHAR(c);
				PUTCHAR('\n');
				PUTCHAR('\r');
			}
		} while (success);


	while(1);

	PROGRAM program;

	/*
	MY_PROTOCOL protocol;

	uint8_t data2[MESSAGE_BUFFER_SIZE];
	uint8_t odosielatel;

	uint8_t data[] = {com_mot_movement, 0xFF, 0xFF, 0xFF, 0xC0};
	protocol.sendMessage(ELEVATOR_MOTOR, data, 5);
	while(1) {
		protocol.sendComand(ELEVATOR_MOTOR, com_mot_sendMotorEncoderCount);
		uint8_t len = protocol.receivedMessage(data2, &odosielatel);
		if(odosielatel == ELEVATOR_MOTOR) {
			protocol.sendMessage(ELEVATOR_TERMINAL, data2, len);
			protocol.sendComand(LED_CABIN_1, com_led_on);
		}
		protocol.sendComand(LED_CABIN_2, com_led_on);
	}
	*/

	/*
	MY_PROTOCOL protocol;

	uint8_t data[MAX_MESSAGE_SIZE];
	protocol.sendComand(ELEVATOR_WATCHDOG_TIMER, com_wdt_timeOutResetTimer);
	protocol.sendComand(ELEVATOR_EMERGENCY_BREAK, com_emb_deactivateBreak);
	while (1) {
		protocol.sendComand(ELEVATOR_WATCHDOG_TIMER, com_wdt_regularyResetTimer);
		protocol.receivedMessage(data);
	}
	*/

	/*
	MY_PROTOCOL protocol;

	uint8_t data[MAX_MESSAGE_SIZE];

	uint8_t buf[] = {com_mot_movement, 0xFF,0xFF,0xFF,0xEC};
	//int* r = (int*)(&buf[1]);
	//*r=-5;
	//buf[4] = buf[1];
	//buf[1] = 0xFF;
	protocol.sendMessage(ELEVATOR_MOTOR, buf, 5);

	while(1) {
		uint8_t source_address;
		protocol.receivedMessage(data, &source_address);
		if(source_address == BUTTON_HALL_1) protocol.sendComand(LED_HALL_1, com_led_on);
		if(source_address == BUTTON_HALL_P) protocol.sendComand(LED_HALL_1, com_led_off);
		if(source_address == LIMIT_SWITCH_3) protocol.sendComand(ELEVATOR_MOTOR, com_mot_stop);
		if(source_address == LIMIT_SWITCH_2) protocol.sendComand(ELEVATOR_MOTOR, com_mot_stop);
		if(source_address == LIMIT_SWITCH_1) protocol.sendComand(ELEVATOR_MOTOR, com_mot_stop);
		if(source_address == LIMIT_SWITCH_P) protocol.sendComand(ELEVATOR_MOTOR, com_mot_stop);

	}
	/*

	/*
	while(1){
			protocol.sendComand(LED_CABIN_1, com_led_on);
			//for(int a= 0; a<0x2FFFFF;a++);

				data[0] = GETCHAR();// DbgConsole_Getchar();
				if(data[0] == START_MESSAGE){
					data[1] = GETCHAR();
					data[2] = GETCHAR();
					data[3] = GETCHAR();
					if(!data[1] && !data[2] && !data[3]) {
						protocol.sendComand(LED_CABIN_2, com_led_on);
						for(int a= 0; a<0x2FFFFF;a++);
						protocol.sendComand(LED_CABIN_2, com_led_off);
					} else {
						protocol.sendComand(LED_CABIN_3, com_led_on);
						for(int a= 0; a<0x2FFFFF;a++);
						protocol.sendComand(LED_CABIN_3, com_led_off);
					}
				}

			protocol.sendComand(LED_CABIN_1, com_led_off);

	}
	*/

	/*
	MY_PROTOCOL protocol;
	uint8_t data[MAX_MESSAGE_SIZE];
	while(1){

		data[0] = GETCHAR();// DbgConsole_Getchar();
		if(data[0] == START_MESSAGE){
			data[1] = GETCHAR();
			data[2] = GETCHAR();
			data[3] = GETCHAR();

			protocol.sendComand(ELEVATOR_CABIN, com_cab_lockDoor);
			for(int a= 0; a<0x2FFFFF;a++);


				if(data[2] == BUTTON_HALL_3) {
					uint8_t data2[]= {com_mot_movement, 0xFF, 0xFF , 0xFF , 0xC0};
					protocol.sendMessage(ELEVATOR_MOTOR, data2, 5);
					for(int a= 0; a<0x6FFFFF;a++);
					protocol.sendComand(ELEVATOR_MOTOR, com_mot_stop);
					for(int a= 0; a<0x6FFFFF;a++);
				}



			protocol.sendComand(ELEVATOR_CABIN, com_cab_unlockDoor);
			for(int a= 0; a<0x2FFFFF;a++);
		}
	}
	*/


	//uint8_t data[] = {0x01};
	//protocol.sendMessage(0xf0, data, 1);

	//uint8_t data[] = {0x01};
	//protocol.sendMessage(ELEVATOR_CABIN, data, 1);
	//protocol.sendComand(ELEVATOR_CABIN,  com_cab_lockDoor);

	//for(int a= 0; a<0x2FFFFF;a++);

	//data[0] = 0x00;
	//protocol.sendMessage(ELEVATOR_CABIN, data, 1);
	//protocol.sendComand(ELEVATOR_CABIN, com_cab_unlockDoor);

	//for(int a= 0; a<0x2FFFFF;a++);

	/*
	MY_PROTOCOL protocol;
	uint8_t crcdata[] = {0xf0, 0x01, 0x01};
	char crc = protocol.getCRC(crcdata, 3);
	char datad[] = {0xa0, 0xf0, 0x01, 0x01, 0x01, crc};
	PRINTF(datad);

	// -64 =      00000000  00000000  00000000  00111111
	// -64 = 		01000000   00000000  00000000 10000000

	//uint8_t crcmotordata[] = {0xf1, 0x01 ,0x02, 0x00, 0x00 , 0x00 , 0x40 };
	//crc = protocol.getCRC(crcmotordata, 7 );
	//char motordata[]={0xa0, 0xf1, 0x01 , 0x05 , 0x02, 0x00, 0x00 , 0x00 , 0x40 , crc };

	uint8_t crcmotordata[] = {0xf1, 0x01 ,0x02, 0xFF, 0xFF , 0xFF , 0xC0 };
	crc = protocol.getCRC(crcmotordata, 7 );
	char motordata[]={0xa0, 0xf1, 0x01 , 0x05 , 0x02,  0xFF, 0xFF , 0xFF , 0xC0 , crc };
	PRINTF(motordata);



	char data[]= {0xA0, 0xd0,0x01, 0x05,'A','B', 'C','\n','\r', 0};

	int ch = DbgConsole_Getchar();
	ch = DbgConsole_Getchar();
	ch = DbgConsole_Getchar();

	data[4] = (char) ch;
	//data[4] = GETCHAR;
	//data[6] = GETCHAR;
	//data[6] = GETCHAR;
	 while(1) {
	    		data[5]++;
	    		if (data[5]== 'Z') data[5]= 'A';
	    		uint8_t data2[]={(uint8_t)data[1], (uint8_t)data[2], (uint8_t)data[4], (uint8_t)data[5], (uint8_t)data[6], (uint8_t)data[7], (uint8_t)data[8]};
	    		data[9] = (char)protocol.getCRC(data2,  (uint8_t)(data[3]+2) );
	    		PRINTF(data);
	    		for(int a= 0; a<0x2FFFFF;a++) {
	    			//ch = DbgConsole_Getchar();
	    			//if (ch>0) PRINTF("MAMZNAK\n\r");
	    		}

	    }
	    */
}
/* TODO: insert other include files here. */

/* TODO: insert other definitions and declarations here. */

/*
 * @brief   Application entry point.
 */
int main(void) {

  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();

    PRINTF("Start_ELEVATOR\n\r");

    while(1) testProgram();

    PROGRAM program;

    /* Force the counter to be placed into memory. */
    volatile static int i = 0 ;
    /* Enter an infinite loop, just incrementing a counter. */
    while(1) {
        i++ ;
    }
    return 0 ;
}
